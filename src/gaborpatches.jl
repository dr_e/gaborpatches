# =========================================================== 
# gabor-patches
# --------------------
# This toolbox provides structures and functions for creating 
# gabor-patches (https://en.wikipedia.org/wiki/Gabor_filter). 
# gabor-patches is inspired by the "Online Gabor-patch 
# generator" (https://github.com/smathot/gabor-patch-generator)
# ===========================================================
# Copyright (C) 2022 Matthias Ertl, matthias.ertl@unibe.ch 
#
# gabor-patches is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 
# as published by the Free software Foundation; either 
# version 2 of the License, or (at your option) any later 
# version.
# pptools is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330, 
# Boston, MA 02111-1 307 USA
# ===========================================================
module gaborpatches

using Colors, ImageView, Images
include("cfg_patch.jl")

function get_patch(cfg_patch)
  std_size = convert(Int64, round(cfg_patch.size/cfg_patch.std_dev))
  orientation_rad = deg2rad(cfg_patch.orientation)

  img = ones(4, cfg_patch.size, cfg_patch.size)

  if cfg_patch.contrast > 1 || cfg_patch.contrast < 0
    error("Contrast out of range. Valid values are between 0 and 1")
  end

  contrast = cfg_patch.contrast / 2

  img_sine = get_sine(cfg_patch)
  img_ker = get_kernel(cfg_patch)

  img_1 = img_sine .* img_ker 

  bgc = repeat(cfg_patch.color_background, inner=(1,100,100))
  img = img_1 + bgc .* (ones(4, cfg_patch.size, cfg_patch.size) - img_ker) 

  return img
end

function get_sine(cfg_patch)

  std_size = convert(Int64, round(cfg_patch.size/cfg_patch.std_dev))
  orientation_rad = deg2rad(cfg_patch.orientation)

  img = ones(4, cfg_patch.size, cfg_patch.size)

  if cfg_patch.contrast > 1 || cfg_patch.contrast < 0
    error("Contrast out of range. Valid values are between 0 and 1")
  end

  contrast = cfg_patch.contrast / 2

  for rx = 1:cfg_patch.size
    for ry = 1:cfg_patch.size
      dx = rx - 0.5 * cfg_patch.std_dev * std_size
      dy = ry - 0.5 * cfg_patch.std_dev * std_size
      t = atan(dy, dx) + orientation_rad
      r = sqrt(dx^2 + dy^2)
      x = r * cos(t)
      y = r * sin(t)
      amp = 0.5 + contrast * cos(2 * pi * (x * cfg_patch.frequency + cfg_patch.phase))

      img[1,rx,ry] = cfg_patch.color_1[1] * amp + cfg_patch.color_2[1] * (1 - amp) 
      img[2,rx,ry] = cfg_patch.color_1[2] * amp + cfg_patch.color_2[2] * (1 - amp)
      img[3,rx,ry] = cfg_patch.color_1[3] * amp + cfg_patch.color_2[3] * (1 - amp)
    end
  end

  return img
end

function get_kernel(cfg_patch)

  std_size = convert(Int64, round(cfg_patch.size/cfg_patch.std_dev))

  img = ones(4, cfg_patch.size, cfg_patch.size)

  for rx = 1:cfg_patch.size
    for ry = 1:cfg_patch.size
      dx = rx - 0.5 * cfg_patch.std_dev * std_size
      dy = ry - 0.5 * cfg_patch.std_dev * std_size
      t = atan(dy, dx)
      r = sqrt(dx^2 + dy^2)
      x = r * cos(t)
      y = r * sin(t)

      if cfg_patch.kernel == "none"
        k = 0.5 
      elseif cfg_patch.kernel == "gaussian"
        k = exp(-0.5 * (x/cfg_patch.std_dev)^2 -0.5 * (y/cfg_patch.std_dev)^2)
      elseif cfg_patch.kernel == "linear"
        k = max(0, (0.5 * cfg_patch.size - r) / (0.5 * cfg_patch.size))
      elseif cfg_patch.kernel == "hanning"
        if r > cfg_patch.size / 2
          k = 0
        else
          k = .5 * (1 - cos((2 * pi * (r + cfg_patch.size / 2) / (cfg_patch.size- 1))))
      end
      elseif cfg_patch.kernel == "hamming"
        if r > cfg_patch.size / 2
          k = 0
        else
          k = .54 - 0.46 * cos((2 * pi * (r + cfg_patch.size / 2) / (cfg_patch.size - 1)))
        end
      else
        error("Kernel of type $cfg_patch.kernel not supported!")
      end
    
      img[1,rx,ry] = k 
      img[2,rx,ry] = k  
      img[3,rx,ry] = k  
    end
  end

  return img 
end

function display_patch(img)
  imshow(colorview(RGBA, img))
end

function save_patch(f::String, img)
  png(f)
end

end
