# =========================================================== 
# gabor-patches
# --------------------
# This toolbox provides structures and functions for creating 
# gabor-patches (https://en.wikipedia.org/wiki/Gabor_filter). 
# gabor-patches is inspired by the "Online Gabor-patch 
# generator" (https://github.com/smathot/gabor-patch-generator)
# ===========================================================
# Copyright (C) 2022 Matthias Ertl, matthias.ertl@unibe.ch 
#
# gabor-patches is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 
# as published by the Free software Foundation; either 
# version 2 of the License, or (at your option) any later 
# version.
# pptools is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330, 
# Boston, MA 02111-1 307 USA
# ===========================================================
using Base:@kwdef

@kwdef mutable struct cfg_patch
  size::Int64 = 100
  std_dev::Int64 = 12
  orientation::Float64 = 0.0
  kernel::String = "gaussian"
  frequency::Float64 = 0.1
  phase::Float64 = 0.0
  contrast::Float64 = 1.0
  color_background::Array{Float64} = [0.5; 0.5; 0.5; 1.0]
  color_1::Array{Float64} = [1.0 1.0 1.0]
  color_2::Array{Float64} = [0.0 0.0 0.0]
end

@kwdef struct cfg_ptn
  rx::Int64 = NaN
  ry::Int64 = NaN
  dx::Float64 = NaN
  dy::Float64 = NaN
  t::Float64 = NaN
  r::Float64 = NaN
  x::Float64 = NaN
  y::Float64 = NaN
  amp::Float64 = NaN
  contrast::Float64 = NaN
  orientation_rad::Float64 = NaN
  std_size = NaN
end  

struct color_def
  red::Int
  green::Int
  blue::Int
end
