# gabor-patches

Gabor patches are frequently used as visual stimuli in psychological research. This toolbox allows for creating gabor patches using julia code.
More sophisticated tools can be found elsewhere (http://www.cogsci.nl/pages/gabor-generator.php?option=co)