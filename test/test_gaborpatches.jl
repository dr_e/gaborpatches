# =========================================================== 
# gaborpatches 
# --------------------
# This toolbox provides structures and functions for psycho-
# physical experiments. 
# ===========================================================
# Copyright (C) 2022 Matthias Ertl, matthias.ertl@unibe.ch
#
# gaborpatches is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 
# as published by the Free software Foundation; either 
# version 2 of the License, or (at your option) any later 
# version.
# gaborpatches is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330, 
# Boston, MA 02111-1 307 USA
# ===========================================================

using gaborpatches, Printf, Test

@testset "patches" begin
if true 
  cfg_pic = gaborpatches.cfg_patch()
  pic = gaborpatches.get_patch(cfg_pic)
  gaborpatches.display_patch(pic)
  
  cfg_pic.contrast = .05 
  pic = gaborpatches.get_patch(cfg_pic)
  gaborpatches.display_patch(pic)
  
  cfg_pic.contrast = 1
  cfg_pic.color_2 = [0 0 1]
  cfg_pic.kernel = "none"
  pic = gaborpatches.get_patch(cfg_pic)
  gaborpatches.display_patch(pic)
  
  cfg_pic = gaborpatches.cfg_patch()
  cfg_pic.kernel = "linear"
  pic = gaborpatches.get_patch(cfg_pic)
  gaborpatches.display_patch(pic)

  cfg_pic = gaborpatches.cfg_patch()
  cfg_pic.kernel = "hanning"
  pic = gaborpatches.get_patch(cfg_pic)
  gaborpatches.display_patch(pic)
 
  
  readline()
end 
  
#  cfg_pic.color_1 = [1 0 0]
#  for x = 0:20:360
#    cfg_pic.orientation = x
#    pic = gaborpatches.get_patch(cfg_pic)
#    gaborpatches.display_patch(pic)
#    sleep(1)
#  end


end

